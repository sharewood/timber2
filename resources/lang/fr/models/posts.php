<?php

return array (
  'singular' => 'Article',
  'plural' => 'Articles',
  'fields' =>
  array (
    'id' => 'Id',
    'title' => 'Title',
    'body' => 'Body',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
