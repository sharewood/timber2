require('./bootstrap');

// Require Vue
window.Vue = require('vue').default;

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Register components from Modules
const moduleVueFiles = require.context('../../Modules', true, /\.vue$/i);
moduleVueFiles.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], moduleVueFiles(key).default));

import VueJsonPretty from 'vue-json-pretty';
import 'vue-json-pretty/lib/styles.css';

Vue.component("vue-json-pretty", VueJsonPretty)

Vue.mixin({ methods: { route } });

import VueQuillEditor from 'vue-quill-editor/src/index'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.filter('kb', val => {
    return Math.floor(val/1024);
});

Vue.use(VueQuillEditor, /* { default global options } */)

// Initialize Vue
const app = new Vue({
    el: '#app',
});
