@props(['rows', 'columns', 'routePrefix'])

<table class="w-full whitespace-no-wrap">
    <thead>
    <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
        @foreach($columns as $column)
            <th class="px-4 py-3">{{ $column['title'] }}</th>
        @endforeach
        <th class="px-4 py-3">Actions</th>
    </tr>
    </thead>
    <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
    @foreach($rows as $row)
        <tr class="text-gray-700 dark:text-gray-400">

            @foreach($columns as $key => $column)
                <td class="px-4 py-3 font-semibold">
                    @if($key === 'image' && $row->{$key})
                        <img src="{{ $row->{$key} }}" class="w-8 h-8 rounded-full">
                    @else
                        {{ $row->{$key} }}
                    @endif
                </td>
            @endforeach

            <td class="px-4 py-3 text-sm flex space-x-4">
                <a class="btn" href="{{ route("$routePrefix.edit",$row->id) }}">edit</a>

                {!! Form::open(['method' => 'DELETE','route' => ["$routePrefix.destroy", $row->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $rows->render() }}
