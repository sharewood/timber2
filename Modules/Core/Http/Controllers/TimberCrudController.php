<?php

namespace Modules\Core\Http\Controllers;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilder;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\Project;
use Modules\Core\Repositories\TimberRepository;
use ReflectionClass;

class TimberCrudController extends Controller
{

    protected $repository = TimberRepository::class;

    protected $indexColumns = [
        'id' => [
            'title' => '#',
            'field' => 'id',
        ],
        'title' => [
            'title' => 'Title',
            'field' => 'title',
        ],
        'slug' => [
            'title' => 'Slug',
            'field' => 'slug',
        ],
    ];

    protected $validationRules = [
        'title' => 'required|max:255',
    ];

    protected $with = [];

    public function __construct()
    {
        $this->setLocaleFromQueryParameters(request());
        $this->repository = new $this->repository(new $this->model);

        //$this->middleware('permission:post-list|post-create|post-edit|post-delete', ['only' => ['index', 'show']]);
        //$this->middleware('permission:post-create', ['only' => ['create', 'store']]);
        //$this->middleware('permission:post-edit', ['only' => ['edit', 'update']]);
        //$this->middleware('permission:post-delete', ['only' => ['destroy']]);

        $this->middleware('permission:post-list');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \ReflectionException
     */
    public function index(Request $request)
    {
        $items = $this->repository->get($this->with);

        return view('core::crud.index', [
            'routePrefix' => $this->getRoutePrefix(),
            'columns' => collect($this->indexColumns),
            'items' => $this->presentIndexItems($items),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $routePrefix = $this->getRoutePrefix();
        return view('core::crud.create', [
            'routePrefix' => $routePrefix
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $routePrefix = $this->getRoutePrefix();

        $validated = $request->validate($this->validationRules);

        //Save basic fields
        $item = $this->model::create($validated);

        $this
            ->saveRelations($request, $item)
            ->saveMedias($request, $item)
            ->saveTags($item);

        return redirect()->route("$routePrefix.edit", [$item, 'locale' => app()->getLocale()])
            ->with('success', 'Post created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->model::find($id);

        return view('core::crud.show', [
            'routePrefix' => $this->getRoutePrefix(),
            'item' => $item,
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id, Request $request)
    {
        $model = $this->model::findOrFail($id);
        $routePrefix = $this->getRoutePrefix();

        return view('core::crud.edit', [
            'model' => $model,
            'routePrefix' => $routePrefix,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $validated = $request->validate($this->validationRules);

        $item = $this->model::findOrFail($id);

        $routePrefix = $this->getRoutePrefix();

        //Save basic fields
        $item->update($validated);

        $this
            ->saveRelations($request, $item)
            ->saveMedias($request, $item)
            ->saveTags($item);

        return redirect()->route("$routePrefix.edit", [$item, 'locale' => app()->getLocale()])->with('success', 'Post updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model::findOrFail($id)->delete();
        $routePrefix = $this->getRoutePrefix();

        return redirect()->route("$routePrefix.index", ['locale' => app()->getLocale()])
            ->with('success', 'Post deleted successfully.');
    }

    /**
     * @param Request $request
     */
    protected function setLocaleFromQueryParameters(Request $request)
    {
        $this->locale = $request->get('locale', app()->getLocale());
        app()->setLocale($this->locale);
    }

    /**
     * @param Request $request
     * @return mixed|string
     */
    protected function getRoutePrefix()
    {
        $requestActionExploded = explode('.', request()->route()->action['as']);

        return $requestActionExploded[0];
    }

    /**
     * Save relations
     * Only works if relation has return type for now
     *
     * @param Request $request
     * @param $model
     */
    public function saveRelations(Request $request, $model): self
    {
        $availableRelations = collect(Project::getAvailableRelations());
        collect($request->all())
            ->filter(function ($value, $key) use ($availableRelations) {
                return $availableRelations->has($key);
            })
            ->map(function ($value, $key) use ($availableRelations, $model) {
                $relationType = $availableRelations[$key];

                if ($relationType === MorphToMany::class) {
                    $model->{$key}()->sync($value);
                }

                if ($relationType === BelongsToMany::class) {
                    $model->{$key}()->sync($value);
                }
            });

        return $this;
    }

    /**
     * @param Request $request
     * @param $model
     */
    public function saveMedias(Request $request, $model): self
    {
        collect($request->allFiles())->each(function ($file, $key) use ($model) {
            if (is_array($file)) {
                collect($file)->each(function ($f) use ($model, $key) {
                    $model->addMedia($f)->toMediaCollection($key);
                });
            } else {
                $model->addMedia($file)->toMediaCollection($key);
            }
        });

        return $this;
    }

    /**
     * @param $item
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function saveTags($item): self
    {
        if (!request()->has('tags')) {
            return $this;
        }

        $item->tags()->sync(request()->get('tags'));

        return $this;
    }

    /**
     * Present index Items for displaying in table (relation, presenters)
     *
     * @param array $indexColumns
     */
    private function presentIndexItems(LengthAwarePaginator $paginator): LengthAwarePaginator
    {
        $paginator->getCollection()->transform(function ($item) {
            foreach ($this->indexColumns as $key => $indexColumn) {
                // If it's a presenter
                if (array_key_exists('presenter', $indexColumn)) {
                    $item->{$key} = $this->{$indexColumn['presenter']}($item);
                }

                //If it's a relation
                if (strpos($indexColumn['field'], '.') !== false) {
                    $exploded = explode('.', $indexColumn['field']);

                    $item->{$key} = $item->{$exploded[0]}->{$exploded[1]};
                }
            }

            return $item;
        });

        return $paginator;
    }
}
