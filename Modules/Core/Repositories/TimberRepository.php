<?php

namespace Modules\Core\Repositories;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Model;
use ReflectionClass;

class TimberRepository{

    /**
     * @var Model
     */
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function get($with = [])
    {
        $query = $this->model->with($with);

        //Load translations
        $reflectionClass = new ReflectionClass($this->model);
        $hasTranslations = $reflectionClass->implementsInterface(TranslatableContract::class);
        if ($hasTranslations) {
            $query = $query->withTranslation()
                ->translatedIn(app()->getLocale());
        }

        $query = $query->latest();

        return $query->paginate(5);
    }
}
