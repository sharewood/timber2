<?php

namespace Modules\Core\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CoreTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_cant_access_to_dashboard()
    {
        $this
            ->get('/dashboard')
            ->assertRedirect(route('login'));
    }
}
