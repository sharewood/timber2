<?php

namespace Modules\Core\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Traits\GetRelationships;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

abstract class TimberModel extends Model implements TranslatableContract, HasMedia{
    use GetRelationships, InteractsWithMedia, HasFactory;

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
