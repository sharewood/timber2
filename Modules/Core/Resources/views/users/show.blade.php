<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Users') }}
            </h2>
        </div>
    </x-slot>

    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">User
                    @can('role-create')
                        <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('users.index') }}">Back</a>
                    </span>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="lead">
                        <strong>Name:</strong>
                        {{ $user->name }}
                    </div>
                    <div class="lead">
                        <strong>Email:</strong>
                        {{ $user->email }}
                    </div>
                    <div class="lead">
                        <strong>Password:</strong>
                        ********
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
