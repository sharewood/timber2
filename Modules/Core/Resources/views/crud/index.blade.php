<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
        <div>
            <language-switcher :locales="{{ json_encode(config('translatable.locales')) }}"
                               locale="{{ app()->getLocale() }}"></language-switcher>
        </div>
    </x-slot>


    <div class="container mx-auto">

        <div class="flex justify-between items-center">
            @can('role-create')
                <a class="btn inline-flex" href="{{ route("$routePrefix.create", ['locale' => app()->getLocale()]) }}">New</a>
            @endcan
        </div>


        <x-data-table :columns="$columns" :rows="$items" :routePrefix="$routePrefix"></x-data-table>

    </div>
</x-app-layout>
