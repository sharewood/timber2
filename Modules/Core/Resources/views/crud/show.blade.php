<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard') }}
            </h2>
        </div>
    </x-slot>

    <div class="container">

        <div class="flex justify-between items-center">
            <div class="text-xl py-2">{{ $item->name }}</div>

            @can('role-create')
                <a class="btn btn-primary" href="{{ route('posts.index') }}">Back</a>
            @endcan
        </div>


        <div>
            @foreach($item->getAttributes() as $key => $value)
                <div class="grid grid-cols-2 gap-10 w-1/2">
                    <div class="font-bold mr-2">{{ $key }} :</div>
                    <div>{{ $value }}</div>
                </div>
            @endforeach
        </div>
    </div>
</x-app-layout>
