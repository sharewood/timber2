<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard') }}
            </h2>
        </div>
    </x-slot>

    <div class="container mx-auto mt-10">

        <form action="{{ route("$routePrefix.update", $model) }}" method="POST" class="my-10 flex"
              enctype="multipart/form-data">
            @method('PUT')
            @csrf
            @include('core::crud.form')
        </form>

        <div>
            <vue-json-pretty :data="{{ $model->load('media') }}"> </vue-json-pretty>
        </div>
    </div>
</x-app-layout>
