<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard') }}
            </h2>
        </div>
    </x-slot>

    <div class="container mx-auto mt-10">
        <form action="{{ route("$routePrefix.store") }}" method="POST" class="my-10 w-full flex"
                  enctype="multipart/form-data">
                @csrf
            @include('core::crud.form')
        </form>
    </div>
</x-app-layout>
