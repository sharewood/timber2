<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Roles') }}
        </h2>
        <div>
            <language-switcher :locales="{{ json_encode(config('translatable.locales')) }}"
                               locale="{{ app()->getLocale() }}"></language-switcher>
        </div>
    </x-slot>

    <div class="container">
        @can('role-create')
            <span class="float-right">
                <a class="btn btn-primary" href="{{ route('roles.create') }}">New Role</a>
            </span>
        @endcan

            <x-data-table :columns="['id', 'name']" :rows="$data"></x-data-table>

        <data-table
            :columns="['id', 'name']"
            :paginator="{{ $data->toJson() }}"
        ></data-table>

        <table class="table table-auto">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th width="280px">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>
                        <a class="btn btn-success" href="{{ route('roles.show',$role->id) }}">Show</a>
                        @can('role-edit')
                            <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                        @endcan
                        @can('role-delete')
                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $data->render() }}
    </div>
</x-app-layout>
