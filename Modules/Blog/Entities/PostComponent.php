<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class PostComponent extends Model
{
    public function componentable()
    {
        return $this->morphTo();
    }
}
