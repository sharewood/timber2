<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Modules\Blog\Database\Factories\PostCategoryFactory;
use Modules\Blog\Database\Factories\PostFactory;
use Modules\Core\Entities\TimberModel;

class PostCategory extends TimberModel implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['title', 'slug'];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return PostCategoryFactory::new();
    }
}
