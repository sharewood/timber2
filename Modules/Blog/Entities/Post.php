<?php

namespace Modules\Blog\Entities;

use Illuminate\View\Component;
use Modules\Blog\Database\Factories\PostFactory;
use Modules\Core\Entities\TimberModel;
use Astrotomic\Translatable\Translatable;


class Post extends TimberModel
{
    use Translatable;

    public $translatedAttributes = [
            'title',
            'body',
            'slug',
            'meta_description',
            'meta_title',
            'meta_keywords',
            'components'
    ];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return PostFactory::new();
    }

    protected $fillable = [
        'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }

    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }


    public function getNextAttribute()
    {
        return self::where('id', '>', $this->id)->orderBy('id')->first();
    }

    public function getPreviousAttribute()
    {
        return self::where('id', '<', $this->id)->orderBy('id', 'desc')->first();
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', true);
    }
}
