<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Tag extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['title', 'slug'];

    protected $fillable = [
        'title',
        'slug'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
