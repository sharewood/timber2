<?php

namespace Modules\Blog\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Casts\AsCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Core\Entities\TimberModel;
use Astrotomic\Translatable\Translatable;

class Project extends TimberModel
{
    use Translatable;

    public $translatedAttributes = [
        'title',
        'body',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    protected $fillable = [
        'title',
        'slug',
        'body',
        'locales',
        'category_id',
        'user_id',
        'featured',
        'chiffres_cles'
    ];

    protected $casts = [
        'chiffres_cles' => AsCollection::class
    ];

    /**
     * Get all of the tags for the project.
     */
    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
