<?php

namespace Modules\Blog\Http\Controllers;


use Modules\Blog\Entities\PostCategory;
use Modules\Blog\Forms\CategoryForm;
use Modules\Core\Http\Controllers\TimberCrudController;

class CategoriesController extends TimberCrudController
{
    protected $model = PostCategory::class;
    protected $form = CategoryForm::class;
}
