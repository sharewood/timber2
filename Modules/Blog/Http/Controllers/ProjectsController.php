<?php

namespace Modules\Blog\Http\Controllers;


use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\Project;
use Modules\Blog\Entities\Tag;
use Modules\Blog\Forms\CategoryForm;
use Modules\Blog\Forms\PostForm;
use Modules\Core\Http\Controllers\TimberCrudController;

class ProjectsController extends TimberCrudController
{
    protected $model = Project::class;

    protected $indexColumns = [
        'id' => [
            'title' => '#',
            'field' => 'id',
        ],
        'image' => [
            'title' => 'Image',
            'field' => 'image',
            'presenter' => 'getFirstMediaUrl'
        ],
        'title' => [
            'title' => 'Title',
            'field' => 'title',
        ],
        'slug' => [
            'title' => 'Slug',
            'field' => 'slug',
        ]
    ];

    protected $validationRules = [
        'title' => 'required|max:255',
        'body' => 'required',
        'category_id' => 'required'
    ];

    public function getFirstMediaUrl(Project $project)
    {
        return $project->getFirstMediaUrl('image');
    }
}
