<?php

namespace Modules\Blog\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\Tag;
use Modules\Blog\Forms\CategoryForm;
use Modules\Blog\Forms\PostForm;
use Modules\Blog\Forms\TagForm;
use Modules\Core\Http\Controllers\TimberCrudController;

class TagsController extends TimberCrudController
{
    protected $model = Tag::class;
    protected $form = TagForm::class;

    protected $indexColumns = [
        'title' => [
            'title' => 'Title',
            'field' => 'title',
        ],
        'slug' => [
            'title' => 'Slug',
            'field' => 'slug'
        ]
    ];
}
