<?php

namespace Modules\Blog\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\PostComponent;
use Modules\Blog\Entities\PostTranslation;
use Modules\Blog\Forms\CategoryForm;
use Modules\Blog\Forms\PostForm;
use Modules\Core\Http\Controllers\TimberCrudController;

class PostsController extends TimberCrudController
{
    protected $model = Post::class;

    protected $validationRules = [
        'title' => 'required|max:255',
        'body' => 'required',
        'category_id' => 'required'
    ];

    protected $indexColumns = [
        'title' => [
            'title' => 'Title',
            'field' => 'title',
        ],
        'category_name' => [
            'title' => 'Category',
            'field' => 'category.title' // relational field
        ]
    ];

    protected $with = ['category'];

    public function edit($id, Request $request)
    {
        $post =  Post::find($id)
            ->load(['category', 'translations.components.componentable']);

        return $post;

        return $post;
    }
}
