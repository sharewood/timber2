<?php

namespace Modules\Blog\Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Modules\Blog\Database\Factories\PostFactory;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\PostCategory;
use Modules\Core\Database\Seeders\CoreDatabaseSeeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class TranslationTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        // first include all the normal setUp operations
        parent::setUp();

        // now re-register all the roles and permissions (clears cache and reloads relations)
        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->artisan('module:seed');
    }


    /** @test */
    public function post_is_saved_in_current_locale()
    {
        /** @var User $admin */
        $admin = User::factory()->isAdmin()->create();

        $this->actingAs($admin);

        $admin->fresh();

        app()->setLocale('en');
        $postData = Post::factory()->make([
            'title' => 'post en'
        ]);
        $this->post(route('posts.store'), $postData->toArray());

        app()->setLocale('fr');
        $postData = Post::factory()->make([
            'title' => 'post fr'
        ]);
        $this
            ->withoutExceptionHandling()
            ->post(route('posts.store'), $postData->toArray());

        $this->assertTrue(Post::whereTranslation('title', 'post en')->first()->hasTranslation('en'));
        $this->assertFalse(Post::whereTranslation('title', 'post en')->first()->hasTranslation('fr'));

        $this->assertTrue(Post::whereTranslation('title', 'post fr')->first()->hasTranslation('fr'));
        $this->assertFalse(Post::whereTranslation('title', 'post fr')->first()->hasTranslation('en'));
    }

    /** @test */
    public function update_post_with_another_locale_create_translation()
    {
        /** @var User $admin */
        $admin = User::factory()->isAdmin()->create();
        $this->actingAs($admin);

        app()->setLocale('fr');
        $post = Post::factory()->create();

        $postEs = Post::factory()->create();

        $this->withoutExceptionHandling()->put(route('posts.update', $post->id), array_merge($postEs->toArray(), ['locale' => 'es']));

        $this->assertTrue(Post::first()->hasTranslation('fr'));
        $this->assertTrue(Post::first()->hasTranslation('es'));
    }
}
