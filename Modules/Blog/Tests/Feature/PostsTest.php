<?php

namespace Modules\Blog\Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Modules\Blog\Entities\Post;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class PostsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // seed the database
        $this->artisan('db:seed');
        $this->artisan('module:seed');
    }

    /** @test */
    public function admin_can_create_post()
    {
        /** @var User $admin */
        $admin = User::factory()->isAdmin()->create();

        $this->actingAs($admin);

        $postData = Post::factory()->make();

        $this->withoutExceptionHandling()->post(route('posts.store'), $postData->toArray());

        $post = Post::first();

        $this->assertEquals($postData->title, $post->title);
        $this->assertEquals($postData->body, $post->body);
        $this->assertEquals(Str::slug($post->title), $post->slug);
    }
}
