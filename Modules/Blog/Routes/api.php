<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Blog\Entities\Post;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/blog', function (Request $request) {
    return $request->user();
});

Route::get('posts', function(Request $request){

    $filter = $request->get('filter');
    $sort = $request->get('sort');

    $data =  DB::table('posts')
        ->join('post_translations', 'posts.id', '=', 'post_translations.post_id');

    if($request->get('filter')){
        $data->where('title', 'like', '%'.$filter.'%');
    }

    return $data->paginate(4);
});
