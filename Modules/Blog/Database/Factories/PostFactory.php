<?php

namespace Modules\Blog\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\PostCategory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'body' => $this->faker->realText(),
            'category_id' => function () {
                return PostCategory::factory()->create()->id;
            }
        ];
    }
}
