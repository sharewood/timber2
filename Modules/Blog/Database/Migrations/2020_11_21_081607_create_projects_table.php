<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function(Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('post_categories');

            $table->json('chiffres_cles')->nullable();

            $table->timestamps();
        });

        Schema::create('project_translations', function(Blueprint $table) {
            $table->timber();
            $table->seo();

            $table->bigIncrements('id');
            $table->bigInteger('project_id')->unsigned();
            $table->text('body');
            $table->boolean('featured')->default(false);

            $table->unique(['project_id', 'locale']);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_translations');
        Schema::dropIfExists('projects');
    }
}
