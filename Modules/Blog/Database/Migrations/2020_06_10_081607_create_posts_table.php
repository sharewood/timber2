<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function(Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('post_categories');

            $table->timestamps();
        });

        Schema::create('post_translations', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->timber();
            $table->seo();

            $table->text('body');

            $table->unsignedBigInteger('post_id')->unsigned();

            $table->unique(['post_id', 'locale']);
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });

        Schema::create('post_components', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('componentable_type');
            $table->unsignedBigInteger('componentable_id');

            $table->unsignedBigInteger('post_translation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_translations');
        Schema::dropIfExists('posts');
        Schema::dropIfExists('post_components');
    }
}
