<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        Schema::create('post_category_translations', function(Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('post_category_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->string('slug')->unique();

            $table->unique(['post_category_id', 'locale']);
            $table->foreign('post_category_id')->references('id')->on('post_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_category_translations');
        Schema::dropIfExists('post_categories');
    }
}
