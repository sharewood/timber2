@if(isset($model))
    <form action="/dashboard/projects/{{ $model->id }}" method="POST" class="my-10 flex" enctype="multipart/form-data">
        @method('PUT')
        @else
            <form action="/dashboard/projects" method="POST" class="my-10 w-full flex" enctype="multipart/form-data">
                @endif
                @csrf
                <div class="w-2/3 mr-10">
                    <tabs>

                        <tab title="Contenu">
                            <input-field
                                name="title"
                                label="Title"
                                required
                                value="{{ old('title', isset($model) ? $model->title : '') }}"
                            ></input-field>

                            <textarea-field
                                name="body"
                                label="Body"
                                required
                                value="{{ old('body', isset($model) ? $model->body : '') }}"
                            ></textarea-field>

                            <wysiwyg-field
                                name="textarea"
                                label="Textarea"
                                required
                                value="{{ old('textarea', isset($model) ? $model->body : '') }}"
                            >
                            </wysiwyg-field>

                            <select-field
                                name="category_id"
                                label="Categorie"
                                required
                                value="{{ old('category_id', isset($model) ? $model->category_id : '') }}"
                                :options="{{ \Modules\Blog\Entities\PostCategory::translatedIn(app()->getLocale())->get()->pluck('title', 'id') }}"
                            >
                            </select-field>
                        </tab>

                        <tab title="Images">
                            <input type="file" name="image">
                            <input type="file" name="images[]" multiple>
                        </tab>

                        <tab title="SEO">
                            <input-field
                                name="meta_title"
                                label="Meta title"
                                required
                                value="{{ old('meta_title', isset($model) ? $model->meta_title : '') }}"
                            ></input-field>
                            <input-field
                                name="meta_description"
                                label="Meta description"
                                required
                                value="{{ old('meta_description', isset($model) ? $model->meta_description : '') }}"
                            ></input-field>
                        </tab>

                        <tab title="Repeater">
                            <!--Repeater JSON-->
                            <repeater-field
                                label="Mon repeater en Json"
                                :items="{{ (isset($model) && $model->chiffres_cles) ? $model->chiffres_cles : new \Illuminate\Support\Collection() }}"
                            >
                                <template v-slot="{item, number}">
                                    <input-field
                                        :name="'chiffres_cles[' + number + '][title]'"
                                        label="Title"
                                        required
                                        v-model="item.title"
                                    ></input-field>

                                    <textarea-field
                                        label="Description"
                                        required
                                        :name="'chiffres_cles[' + number + '][description]'"
                                        v-model="item.description"
                                    ></textarea-field>
                                </template>
                            </repeater-field>
                        </tab>

                    </tabs>

                </div>
                <div class="w-1/3">
                    <div class="bg-white px-10 py-5 sticky top-0">
                        <select-field
                            name="locale"
                            label="Locale"
                            required
                            value="{{ app()->getLocale() }}"
                            :options="{{ collect(app('translatable.locales')->all())->map(function ($locale) {return [
                                    $locale => $locale
                                ];
                            })->collapse() }}">
                        </select-field>

                        <button class="btn mt-2">Submit</button>
                    </div>
                </div>
            </form>
