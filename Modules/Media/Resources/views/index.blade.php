<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Médias') }}
            </h2>
        </div>
    </x-slot>

    <div class="container">
        {{ $medias }}
    </div>
</x-app-layout>
