<?php

namespace Timber\Admin;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Timber\Timber\Skeleton\SkeletonClass
 */
class TimberFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'timber';
    }
}
